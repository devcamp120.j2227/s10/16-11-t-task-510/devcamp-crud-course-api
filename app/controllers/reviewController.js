const mongoose = require("mongoose");

const reviewModel = require("../models/reviewModel");
const courseModel = require("../models/courseModel");

const createReview = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const courseId = request.params.courseId;
    const body = request.body;
    console.log(courseId);
    // 63748ac967a684873968b188
    console.log(body);
    // { bodyStars: 5, bodyNote: 'Good' }

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

    if (!(Number.isInteger(body.bodyStars) && body.bodyStars > 0 && body.bodyStars <= 5)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Rate không hợp lệ"
        })
    }

    // B3: Thao tác với CSDL
    const newReview = {
        _id: mongoose.Types.ObjectId(),
        stars: body.bodyStars,
        note: body.bodyNote
    }
    console.log(newReview);
    // {
    //     _id: new ObjectId("6374ded1ac766a85a1631b0d"),
    //     stars: 5,
    //     note: 'Good'
    // }

    reviewModel.create(newReview, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        // Thên ID của review mới được tạo vào trong mảng reviews của course truyền tại params
        courseModel.findByIdAndUpdate(courseId, {
            $push: {
                reviews: data._id
            }
        }, (err, updatedCourse) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(201).json({
                status: "Create course successfully",
                data: data
            })
        })
    })
}

const getReviewById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const reviewId = request.params.reviewId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "ReviewId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    reviewModel.findById(reviewId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail review successfully",
            data: data
        })
    })
}

const updateReviewById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const reviewId = request.params.reviewId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "ReviewID không hợp lệ"
        })
    }

    if(body.bodyStars !== undefined && !(Number.isInteger(body.bodyStars) && body.bodyStars > 0 && body.bodyStars <= 5)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Rate không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateReview = {}

    if(body.bodyStars !== undefined) {
        updateReview.stars = body.bodyStars
    }

    if(body.bodyNote !== undefined) {
        updateReview.note = body.bodyNote
    }

    reviewModel.findByIdAndUpdate(reviewId, updateReview, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update course successfully",
            data: data
        })
    })
}

const getAllReview = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    reviewModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all reviews successfully",
            data: data
        })
    })
}

const getAllReviewOfCourse = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const courseId = request.params.courseId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    courseModel.findById(courseId)
        .populate("reviews")
        .exec((error, data) => {
            if(error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
    
            return response.status(200).json({
                status: "Get all reviews of course successfully",
                data: data
            })
        })
}

const deleteReview = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const reviewId = request.params.reviewId;
    const courseId = request.params.courseId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "ReviewId không hợp lệ"
        })
    }

    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    reviewModel.findByIdAndDelete(reviewId, (error) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        courseModel.findByIdAndUpdate(courseId, {
           $pull: { reviews: reviewId } 
        }, (err) => {
            if(err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(200).json({
                status: "Delete review successfully"
            })
        })
    })
}

module.exports = {
    createReview,
    getReviewById,
    updateReviewById,
    getAllReview,
    getAllReviewOfCourse,
    deleteReview
}