// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import review middleware
const reviewMiddleware = require("../middlewares/reviewMiddleware");

// Import review controller
const reviewController = require("../controllers/reviewController");

router.get("/reviews", reviewMiddleware.getAllReviewMiddleware, reviewController.getAllReview);

router.post("/courses/:courseId/reviews", reviewMiddleware.createReviewMiddleware, reviewController.createReview);

router.get("/courses/:courseId/reviews", reviewMiddleware.getAllReviewMiddleware, reviewController.getAllReviewOfCourse);

router.get("/reviews/:reviewId", reviewMiddleware.getDetailReviewMiddleware, reviewController.getReviewById);

router.put("/reviews/:reviewId", reviewMiddleware.updateReviewMiddleware, reviewController.updateReviewById);

router.delete("/courses/:courseId/reviews/:reviewId", reviewMiddleware.deleteReviewMiddleware, reviewController.deleteReview);

module.exports = router;